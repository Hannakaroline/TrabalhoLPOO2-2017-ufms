package br.ufms.facom.lpoo.rpg.mecanicas.arma;

public class EspadaLonga implements Arma {

	@Override
	public int getAlcance() {
		return 2;
	}

}
