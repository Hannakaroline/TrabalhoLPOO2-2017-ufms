package br.ufms.facom.lpoo.rpg.mecanicas.arma;

public class ArcoFlecha implements Arma {

	@Override
	public int getAlcance() {
		return 4;
	}

}
