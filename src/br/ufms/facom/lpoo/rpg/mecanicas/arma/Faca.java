package br.ufms.facom.lpoo.rpg.mecanicas.arma;

public class Faca implements Arma {

    @Override
    public int getAlcance() {
        return 1;
    }

}
