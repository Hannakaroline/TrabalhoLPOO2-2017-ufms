package br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes;

public class ExcecaoDeAtributoInvalido extends Exception {
    public ExcecaoDeAtributoInvalido(String s) {
        super(s);
    }
}
