package br.ufms.facom.lpoo.rpg.mecanicas.personagem;

import java.util.Objects;

/**
 * Armazena uma posição no tabuleiro (casa).
 * <p>
 * Uma posição é representada por um índice horizontal (x) e um índice vertical
 * (y). Ambos os índices devem ser números inteiros no intervalo [0,7].
 * 
 * @author eraldo
 *
 */
public class Posicao {
	/**
	 * Índice horizontal (coluna).
	 */
	public int x;

	/**
	 * Índice vertical (linha).
	 */
	public int y;

	/**
	 * Cria uma posição que corresponde à casa (0,0).
	 */
	public Posicao() {
	}

	/**
	 * Cria uma posição que corresponde à casa (x,y).
	 * 
	 * @param x
	 *            índice horizontal.
	 * @param y
	 *            índice vertical.
	 */
	public Posicao(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 *
	 * @param posicao para calcular distância
	 * @return distância manhatan até posição.
	 */
	public int distanciaManhatan(Posicao posicao) {

		return Math.abs(this.x - posicao.x) + Math.abs(this.y - posicao.y);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Posicao posicao = (Posicao) o;
		return x == posicao.x &&
				y == posicao.y;
	}

	@Override
	public int hashCode() {

		return Objects.hash(x, y);
	}
}
