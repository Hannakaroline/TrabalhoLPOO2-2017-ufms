package br.ufms.facom.lpoo.rpg.mecanicas.personagem.tipos;

import br.ufms.facom.lpoo.rpg.mecanicas.arma.Arma;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.Personagem;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.Posicao;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeArmaIncompativel;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeAtributoInvalido;

import java.util.Objects;

public abstract class PersonagemBase implements Personagem {

    private final String nome;
    private final int ataque;
    private final int defesa;
    private final int velocidade;
    private final Arma arma;
    private Posicao posicao;
    private int vida;

    public PersonagemBase(String nome, int ataque, int defesa, int velocidade, Arma arma) throws ExcecaoDeAtributoInvalido, ExcecaoDeArmaIncompativel {
        this.nome = nome;
        this.ataque = ataque;
        this.defesa = defesa;
        this.velocidade = velocidade;
        this.arma = arma;
        this.vida = 5;
        this.posicao = new Posicao();

        validarAtributos();
        if(arma.getAlcance() > 1 && !personagemPodeUsarArma(arma)) {
                throw new ExcecaoDeArmaIncompativel();
        }
    }

    protected abstract boolean personagemPodeUsarArma(Arma arma);

    private void validarAtributos() throws ExcecaoDeAtributoInvalido {
        validarNome(nome);
        validarAtributo("Ataque", ataque);
        validarAtributo("Defesa", defesa);
        validarAtributo("Velocidade" , velocidade);
    }

    private void validarAtributo(String campo, int ataque) throws ExcecaoDeAtributoInvalido {
        if (ataque < 1 || ataque > 5){
            throw new ExcecaoDeAtributoInvalido(String.format("o %s deve estar entre 1 e 5", campo));
        }
    }

    private void validarNome(String nome) throws ExcecaoDeAtributoInvalido {
        if(nome == null || nome.length() == 0){
            throw new ExcecaoDeAtributoInvalido("Nome nao pode ser vazio");
        }
    }

    @Override
    public int getAtaque() {
        return ataque;
    }

    @Override
    public String getNome() {
        return nome;
    }

    @Override
    public int getDefesa() {
        return defesa;
    }

    @Override
    public int getVelocidade() {
        return velocidade;
    }

    @Override
    public void setVida(int vida) {
        this.vida = vida;
    }

    @Override
    public int getVida() {
        return vida;
    }

    @Override
    public int getX() {
        return posicao.x;
    }

    @Override
    public int getY() {
        return posicao.y;
    }

    @Override
    public void setX(int x) {
        this.posicao.x = x;
    }

    @Override
    public void setY(int y) {
        this.posicao.y = y;
    }

    @Override
    public Arma getArma() {
        return arma;
    }

    @Override
    public boolean alcancaComArma(Personagem defendente) {
        return distanciaAte(defendente) <= arma.getAlcance();
    }

    public void setPosicao(Posicao posicao) {
        this.posicao = posicao;
    }

    public Posicao getPosicao() {
        return posicao;
    }

    public boolean alcanca(Posicao destino) {
        return this.posicao.distanciaManhatan(destino) <= velocidade;
    }

    private int distanciaAte(Personagem defendente) {
        return posicao.distanciaManhatan(new Posicao(defendente.getX(), defendente.getY()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonagemBase that = (PersonagemBase) o;
        return Objects.equals(nome, that.nome) &&
                Objects.equals(posicao, that.posicao);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nome, posicao);
    }
}
