package br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes;

public class ExcecaoDeArmaIncompativel extends Exception {

    public ExcecaoDeArmaIncompativel() {
        super("Arma incompativel com o personagem");
    }
}
