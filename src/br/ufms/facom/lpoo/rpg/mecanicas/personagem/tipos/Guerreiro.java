package br.ufms.facom.lpoo.rpg.mecanicas.personagem.tipos;

import br.ufms.facom.lpoo.rpg.mecanicas.arma.Arma;
import br.ufms.facom.lpoo.rpg.mecanicas.arma.EspadaLonga;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeArmaIncompativel;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeAtributoInvalido;

public abstract class Guerreiro extends PersonagemBase {

	public Guerreiro(String nome, Arma arma) throws ExcecaoDeAtributoInvalido, ExcecaoDeArmaIncompativel {
		super(nome, 3,3, 3, arma);
	}

	@Override
	protected boolean personagemPodeUsarArma(Arma arma) {
		return arma instanceof EspadaLonga;
	}
}
