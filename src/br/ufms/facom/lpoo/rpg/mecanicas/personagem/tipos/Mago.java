package br.ufms.facom.lpoo.rpg.mecanicas.personagem.tipos;

import br.ufms.facom.lpoo.rpg.mecanicas.arma.Arma;
import br.ufms.facom.lpoo.rpg.mecanicas.arma.CajadoMagico;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeArmaIncompativel;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeAtributoInvalido;

public abstract class Mago extends PersonagemBase {

	public Mago(String nome, Arma arma) throws ExcecaoDeAtributoInvalido, ExcecaoDeArmaIncompativel {
		super(nome, 5, 1, 3, arma);
	}

	@Override
	protected boolean personagemPodeUsarArma(Arma arma) {
		return arma instanceof CajadoMagico;
	}
}
