package br.ufms.facom.lpoo.rpg.mecanicas.personagem.tipos;

import br.ufms.facom.lpoo.rpg.mecanicas.arma.Arma;
import br.ufms.facom.lpoo.rpg.mecanicas.arma.Machadinha;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeArmaIncompativel;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeAtributoInvalido;

public abstract class Barbaro extends PersonagemBase {

    public Barbaro(String nome, Arma arma) throws ExcecaoDeAtributoInvalido, ExcecaoDeArmaIncompativel {
        super(nome, 4, 2, 4, arma);
    }

    @Override
    protected boolean personagemPodeUsarArma(Arma arma) {
        return arma instanceof Machadinha;
    }
}
