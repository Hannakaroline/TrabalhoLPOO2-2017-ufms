package br.ufms.facom.lpoo.rpg.historia.amigos;

import br.ufms.facom.lpoo.rpg.mecanicas.arma.Arma;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeArmaIncompativel;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeAtributoInvalido;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.tipos.Mago;

public class Affonso extends Mago {
    public Affonso(Arma arma) throws ExcecaoDeAtributoInvalido, ExcecaoDeArmaIncompativel {
        super("Affonso", arma);
    }
}
