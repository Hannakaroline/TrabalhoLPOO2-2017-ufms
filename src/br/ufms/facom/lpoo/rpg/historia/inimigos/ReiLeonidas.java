package br.ufms.facom.lpoo.rpg.historia.inimigos;

import br.ufms.facom.lpoo.rpg.mecanicas.arma.Arma;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeArmaIncompativel;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeAtributoInvalido;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.tipos.Barbaro;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.tipos.Mago;

public class ReiLeonidas extends Barbaro {
    public ReiLeonidas(Arma arma) throws ExcecaoDeAtributoInvalido, ExcecaoDeArmaIncompativel {
        super("Rei Leonidas", arma);
    }
}
