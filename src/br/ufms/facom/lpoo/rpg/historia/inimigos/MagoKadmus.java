package br.ufms.facom.lpoo.rpg.historia.inimigos;

import br.ufms.facom.lpoo.rpg.mecanicas.arma.Arma;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeArmaIncompativel;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeAtributoInvalido;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.tipos.Mago;

public class MagoKadmus extends Mago {
    public MagoKadmus(Arma arma) throws ExcecaoDeAtributoInvalido, ExcecaoDeArmaIncompativel {
        super("Mago Kadmus", arma);
    }
}
