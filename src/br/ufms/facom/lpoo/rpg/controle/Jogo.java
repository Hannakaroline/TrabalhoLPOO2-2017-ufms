package br.ufms.facom.lpoo.rpg.controle;

import br.ufms.facom.lpoo.rpg.historia.amigos.Affonso;
import br.ufms.facom.lpoo.rpg.historia.amigos.Aurora;
import br.ufms.facom.lpoo.rpg.historia.amigos.Thamires;
import br.ufms.facom.lpoo.rpg.historia.amigos.Tobias;
import br.ufms.facom.lpoo.rpg.historia.inimigos.ConselheiroSabin;
import br.ufms.facom.lpoo.rpg.historia.inimigos.CrocoJoe;
import br.ufms.facom.lpoo.rpg.historia.inimigos.MagoKadmus;
import br.ufms.facom.lpoo.rpg.historia.inimigos.ReiLeonidas;
import br.ufms.facom.lpoo.rpg.mecanicas.arma.*;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.Personagem;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.Posicao;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeArmaIncompativel;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeAtributoInvalido;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.tipos.PersonagemBase;
import br.ufms.facom.lpoo.rpg.ui.RolePlayingGame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Jogo {
    private final RolePlayingGame rpg;
    private boolean jogoAcabou = false;
    private Random mestreRandomico = new Random();
    private List<PersonagemBase> amigos = new ArrayList<>();
    private List<PersonagemBase> inimigos = new ArrayList<>();

    public Jogo(RolePlayingGame rpg) throws ExcecaoDeArmaIncompativel, ExcecaoDeAtributoInvalido {
        this.rpg = rpg;
        iniciaAmigos();
        iniciaInimigos();
    }

    /**
     * inicia os personagens do time dos amigos
     * @throws ExcecaoDeArmaIncompativel caso algum personagem tenha sido criado com uma arma incompativel
     * @throws ExcecaoDeAtributoInvalido caso algum personagem tenha sido declarado com atributos invalidos
     */
    private void iniciaAmigos() throws ExcecaoDeArmaIncompativel, ExcecaoDeAtributoInvalido {
        PersonagemBase aurora = new Aurora(new EspadaLonga());
        aurora.setPosicao(new Posicao(0, 0));

        PersonagemBase affonso = new Affonso(new CajadoMagico());
        affonso.setPosicao(new Posicao(1, 0));

        PersonagemBase thamires = new Thamires(new ArcoFlecha());
        thamires.setPosicao(new Posicao(2, 0));

        PersonagemBase tobias = new Tobias(new Machadinha());
        tobias.setPosicao(new Posicao(3, 0));

        amigos.addAll(Arrays.asList(aurora, affonso, thamires, tobias));
        for (Personagem personagem : amigos) {
            rpg.addPersonagem(personagem);
        }
    }

    /**
     * inicia os personagens do time dos inimigos
     * @throws ExcecaoDeArmaIncompativel caso algum personagem tenha sido criado com uma arma incompativel
     * @throws ExcecaoDeAtributoInvalido caso algum personagem tenha sido declarado com atributos invalidos
     */
    private void iniciaInimigos() throws ExcecaoDeArmaIncompativel, ExcecaoDeAtributoInvalido {
        PersonagemBase conselheiroSabin = new ConselheiroSabin(new EspadaLonga());
        conselheiroSabin.setPosicao(new Posicao(7, 7));

        PersonagemBase crocJoe = new CrocoJoe(new Faca());
        crocJoe.setPosicao(new Posicao(6, 7));

        PersonagemBase kadmus = new MagoKadmus(new CajadoMagico());
        kadmus.setPosicao(new Posicao(5, 7));

        PersonagemBase reiLeonidas = new ReiLeonidas(new Machadinha());
        reiLeonidas.setPosicao(new Posicao(4, 7));

        inimigos.addAll(Arrays.asList(conselheiroSabin, crocJoe, kadmus, reiLeonidas));
        for (Personagem personagem : inimigos) {
            rpg.addPersonagem(personagem);
        }
    }

    public boolean naoAcabou() {
        return !jogoAcabou;
    }

    public void anuncia(String anuncio) {
        this.rpg.info(anuncio);
    }

    public List<PersonagemBase> getAmigos() {
        return amigos;
    }

    public Posicao selecionaPosicaoDoTabuleiro() throws InterruptedException {
        return this.rpg.selecionaPosicao();
    }

    public void anunciaErro(String anuncio) {
        this.rpg.erro(anuncio);
    }

    public List<PersonagemBase> getInimigos() {
        return inimigos;
    }

    public void atualizaTabuleiro() {
        this.rpg.atualizaTabuleiro();
    }

    public Personagem playerSelecionaPersonagem() throws InterruptedException {
        return this.rpg.selecionaPersonagem();
    }

    public boolean personagemEhInimigo(Personagem personagem) {
        return inimigos.contains(personagem);
    }

    public boolean acabou() {
        return jogoAcabou;
    }

    public void executarAtaque(Personagem atacante, Personagem defendente) {
        anuncia(String.format("Atacante: %s | Defendente: %s", atacante.getNome(), defendente.getNome()));
        if(atacante.alcancaComArma(defendente)){
            int probabilidadeDeSucessoDoAtaqueR = 5 + atacante.getAtaque() - defendente.getDefesa();
            int sorteA = mestreRandomico.nextInt(10) + 1;
            anuncia(String.format("Probablidade de Sucesso Do Ataque R: %d", probabilidadeDeSucessoDoAtaqueR));
            anuncia(String.format("Número randômico sorteado A: %d", sorteA));

            if(sorteA < probabilidadeDeSucessoDoAtaqueR) {
                anuncia("Ataque bem sucedido");
                defendente.setVida(defendente.getVida() - 1);
            } else {
                anunciaErro("Ataque falhou");
            }
        } else {
            anunciaErro("Ataque falhou. Inimigo Fora de alcance.");
        }
        checarVidaDeAmigo(defendente);
        checarVidaDeInimigo(defendente);
        rpg.atualizaTabuleiro();
    }

    private void checarVidaDeInimigo(Personagem personagem) {
        checarVidaDe(personagem, inimigos);
        checarCondicoesDeVitoria();
    }

    private void checarVidaDeAmigo(Personagem personagem) {
        checarVidaDe(personagem, amigos);
        checarCondicoesDeDerrota();
    }

    private void checarVidaDe(Personagem personagem, List<PersonagemBase> listaDoLadoDoPersonagem) {
        if(personagem.getVida() <= 0 && listaDoLadoDoPersonagem.contains(personagem)) {
            rpg.removePersonagem(personagem);
            listaDoLadoDoPersonagem.remove(personagem);
            rpg.info(String.format("%s foi executado!", personagem.getNome()));
            rpg.atualizaTabuleiro();
        }
    }

    private void checarCondicoesDeDerrota() {
        if(amigos.size() == 0) {
            rpg.info("GAME OVER, todos seus companheiros foram executados :(");
            jogoAcabou = true;
        }
    }

    private void checarCondicoesDeVitoria() {
        if(inimigos.size() == 0) {
            rpg.info("Parabéns, o time dos amigos venceu! :) ");
            jogoAcabou = true;
        }
    }
}
