package br.ufms.facom.lpoo.rpg.controle;

import br.ufms.facom.lpoo.rpg.controle.subcontroladores.ControladorDeTurnoDosAmigos;
import br.ufms.facom.lpoo.rpg.controle.subcontroladores.ControladorDeTurnoDosInimigos;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.*;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeArmaIncompativel;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.excecoes.ExcecaoDeAtributoInvalido;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.tipos.PersonagemBase;
import br.ufms.facom.lpoo.rpg.ui.RolePlayingGame;
import com.sun.istack.internal.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Controle do jogo: personagens e suas interações.
 * <p>
 * O intuito desta implementação é apenas exemplificar o uso dos principais
 * métodos da classe de interface: <code>RolePlayingGame</code>. A implementação
 * apresentada aqui não condiz com os requisitos do trabalho (apenas um tipo de
 * personagem (<code>Soldado</code>) e um tipo de arma (<code>Faca</code>) são
 * implementados aqui). Apenas dois personagens (do mesmo tipo) são adicionados
 * ao tabuleiro e a interação entre eles não respeita as regras do trabalho.
 * 
 * @author eraldo
 *
 */
public class Controle {

	/**
	 * Interface gráfica do jogo. Fornece métodos de entrada e saída.
	 */
	private RolePlayingGame rpg;
	private final ControladorDeTurnoDosAmigos controladorDosAmigos;
	private final ControladorDeTurnoDosInimigos controladorDosInimigos;
	private final Jogo jogo;

	/**
	 * Cria um objeto de controle que usa o objeto <code>rpg</code> como
	 * interface com o usuário.
	 * 
	 * @param rpg
	 *            interface gráfica da aplicação.
	 *
	 */
	public Controle(RolePlayingGame rpg) throws ExcecaoDeArmaIncompativel, ExcecaoDeAtributoInvalido {
		this.rpg = rpg;
		this.jogo = new Jogo(rpg);
		this.controladorDosAmigos = new ControladorDeTurnoDosAmigos();
		this.controladorDosInimigos = new ControladorDeTurnoDosInimigos();
	}

	/**
	 * Executa um turno do jogo. Este método é invocado pelo interface gráfica
	 * continuamente, enquanto a aplicação estiver rodando.
	 * <p>
	 *
	 * @throws InterruptedException
	 *             Exceção lançada quando a aplicação é encerrada pelo usuário.
	 *             O controle do jogo é executado em uma thread separada da
	 *             thread principal da aplicação. Esta exceção é lançada para
	 *             permitir o encerramento da thread de controle quando ela está
	 *             esperando uma resposta da interface com relação a uma ação do
	 *             usuário (selecionar personagem ou posição). O tratamento
	 *             desta exceção é realizado pela classe da aplicação
	 *             (<code>RolePlayingGame</code>). Esta exceção não deve ser
	 *             capturada aqui.
	 */
	public void executaTurno() throws InterruptedException {
		if (jogo.acabou()) return;
		controladorDosAmigos.executaTurno(jogo);
		controladorDosInimigos.executaTurno(jogo);
	}
}
