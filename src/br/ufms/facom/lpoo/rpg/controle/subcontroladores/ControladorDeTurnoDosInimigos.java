package br.ufms.facom.lpoo.rpg.controle.subcontroladores;

import br.ufms.facom.lpoo.rpg.controle.Jogo;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.Personagem;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.Posicao;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.tipos.PersonagemBase;
import com.sun.istack.internal.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ControladorDeTurnoDosInimigos extends SubControladorBase {

    @Override
    public void executaTurno(Jogo jogo) throws InterruptedException {
        // Turno dos Inimigos
        jogo.anuncia("INICIO DO TURNO DOS INIMIGOS!");
        for (PersonagemBase inimigo : jogo.getInimigos()) {
            Personagem primeiroOponenteAoAlcanceDaArma = procurarPrimeiroOponenteAoAlcanceDaArma(jogo, inimigo);
            if (primeiroOponenteAoAlcanceDaArma != null) {
                jogo.anuncia(String.format("%s Não se mexeu", inimigo.getNome()));
                jogo.executarAtaque(inimigo, primeiroOponenteAoAlcanceDaArma);
                if(jogo.acabou()) break;
            } else {
                PersonagemBase oponenteMaisProximo = procurarOponenteMaisProximo(jogo, inimigo);
                List<Posicao> possiveisPosicoes = obterPossiveisPosicoes(inimigo);
                possiveisPosicoes.sort((posicao1, posicao2) -> posicaoMaisProximaDe(oponenteMaisProximo.getPosicao(), posicao1, posicao2));
                Posicao posicao = selecionarMelhorPosicao(jogo, possiveisPosicoes, inimigo, oponenteMaisProximo);
                inimigo.setPosicao(posicao);
                jogo.atualizaTabuleiro();
                jogo.anuncia(String.format("%s moveu-se para: %d, %d", inimigo.getNome(), inimigo.getX(), inimigo.getY()));
                if (inimigo.alcancaComArma(oponenteMaisProximo)) {
                    jogo.executarAtaque(inimigo, oponenteMaisProximo);
                    if(jogo.acabou()) break;
                } else {
                    jogo.anuncia(String.format("%s nao atacou. ", inimigo.getNome()));
                }
            }

            jogo.atualizaTabuleiro();
        }
    }

    private Posicao selecionarMelhorPosicao(Jogo jogo, List<Posicao> possiveisPosicoes, PersonagemBase personagemOrigem, PersonagemBase oponenteMaisProximo) {
        Posicao posicaoDoOponenteMaisProximo = oponenteMaisProximo.getPosicao();
        for (int i = possiveisPosicoes.size() - 1; i > 0; i--){
            if (possiveisPosicoes.get(i).distanciaManhatan(posicaoDoOponenteMaisProximo) < personagemOrigem.getArma().getAlcance()) {
                if (posicaoEstaLivrePara(jogo, personagemOrigem, possiveisPosicoes.get(i)))
                    return possiveisPosicoes.get(i);
            }
        }
        for (Posicao possiveisPosicao : possiveisPosicoes) {
            if (posicaoEstaLivrePara(jogo, personagemOrigem, possiveisPosicao)) {
                return possiveisPosicao;
            }
        }
        return personagemOrigem.getPosicao();
    }

    private int posicaoMaisProximaDe(Posicao oponenteMaisProximoPosicao, Posicao posicao1, Posicao posicao2) {
        return oponenteMaisProximoPosicao.distanciaManhatan(posicao1) - oponenteMaisProximoPosicao.distanciaManhatan(posicao2);
    }

    private List<Posicao> obterPossiveisPosicoes(PersonagemBase personagem) {
        List<Posicao> listaDePosicoesPossiveis = new ArrayList<>();

        int xMinimo = obterXMinimoDoTabuleiro(personagem);
        int xMaximo = obterXMaximoDoTabuleiro(personagem);
        int yMinimo = obterYMinimaDoTabuleiro(personagem);
        int yMaximo = obterYMaximoDoTabuleiro(personagem);

        for(int x = xMinimo; x <= xMaximo; x++) {
            for (int y = yMinimo; y <= yMaximo; y++) {
                Posicao possivelPosicao = new Posicao(x, y);
                if (personagem.alcanca(possivelPosicao)) {
                    listaDePosicoesPossiveis.add(possivelPosicao);
                }
            }
        }

        return listaDePosicoesPossiveis;
    }

    private int obterXMinimoDoTabuleiro(PersonagemBase inimigo) {
        if (inimigo.getX() - inimigo.getVelocidade() < 0) {
            return 0;
        }
        return inimigo.getX() - inimigo.getVelocidade();
    }

    private int obterXMaximoDoTabuleiro(PersonagemBase inimigo){
        if (inimigo.getX() + inimigo.getVelocidade() > 7) {
            return 7;
        }
        return inimigo.getX() + inimigo.getVelocidade();
    }

    private int obterYMinimaDoTabuleiro(PersonagemBase inimigo) {
        if (inimigo.getY() - inimigo.getVelocidade() < 0) {
            return 0;
        }
        return inimigo.getY() - inimigo.getVelocidade();
    }

    private  int obterYMaximoDoTabuleiro(PersonagemBase inimigo){
        if (inimigo.getY() + inimigo.getVelocidade() > 7) {
            return 7;
        }
        return inimigo.getY() + inimigo.getVelocidade();
    }

    private PersonagemBase procurarOponenteMaisProximo(Jogo jogo, PersonagemBase personagemQuerendoAtacar) {
        Posicao posicaoDoPersonagemQuerendoAtacar = personagemQuerendoAtacar.getPosicao();
        int indexDoPersonagemMaisProximo = 0;
        int distanciaDoPersonagemMaisProximo = jogo.getAmigos().get(0).getPosicao().distanciaManhatan(posicaoDoPersonagemQuerendoAtacar);

        for(int i = 1; i < jogo.getAmigos().size(); i++) {
            int distanciaDoPersonagemAtual = jogo.getAmigos().get(i).getPosicao().distanciaManhatan(posicaoDoPersonagemQuerendoAtacar);
            if (distanciaDoPersonagemAtual < distanciaDoPersonagemMaisProximo) {
                distanciaDoPersonagemMaisProximo = distanciaDoPersonagemAtual;
                indexDoPersonagemMaisProximo = i;
            }
        }

        return jogo.getAmigos().get(indexDoPersonagemMaisProximo);
    }

    @Nullable
    private Personagem procurarPrimeiroOponenteAoAlcanceDaArma(Jogo jogo, PersonagemBase personagemQuerendoAtacar){
        for (PersonagemBase amigo : jogo.getAmigos()) {
            if (personagemQuerendoAtacar.alcancaComArma(amigo)) {
                return amigo;
            }
        }
        return null;
    }
}
