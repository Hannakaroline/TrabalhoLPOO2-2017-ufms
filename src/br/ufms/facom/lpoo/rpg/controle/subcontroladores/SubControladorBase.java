package br.ufms.facom.lpoo.rpg.controle.subcontroladores;

import br.ufms.facom.lpoo.rpg.controle.Jogo;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.Personagem;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.Posicao;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.tipos.PersonagemBase;

public abstract class SubControladorBase implements Subcontrolador {

    public final boolean posicaoEstaLivrePara(Jogo jogo, Personagem personagemEmMovimento, Posicao posicao) {
        for (PersonagemBase personagem : jogo.getAmigos()) {
            if (personagem.getPosicao().equals(posicao) && !personagemEmMovimento.equals(personagem)) return false;
        }
        for (PersonagemBase personagem : jogo.getInimigos()) {
            if (personagem.getPosicao().equals(posicao) && !personagemEmMovimento.equals(personagem)) return false;
        }
        return true;
    }
}
