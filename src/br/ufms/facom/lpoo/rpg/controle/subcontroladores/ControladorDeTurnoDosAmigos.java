package br.ufms.facom.lpoo.rpg.controle.subcontroladores;

import br.ufms.facom.lpoo.rpg.controle.Jogo;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.Personagem;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.Posicao;
import br.ufms.facom.lpoo.rpg.mecanicas.personagem.tipos.PersonagemBase;

public class ControladorDeTurnoDosAmigos extends SubControladorBase {

    @Override
    public void executaTurno(Jogo jogo) throws InterruptedException {
        jogo.anuncia("INICIO DO TURNO DOS AMIGOS");

        for (PersonagemBase amigo: jogo.getAmigos()) {
            jogo.anuncia(String.format("Personagem %s, selecione sua nova posição!", amigo.getNome()));

            /*
             * Solicita uma casa do tabuleiro à interface. O usuário deverá
             * selecionar (clicando com o mouse) em uma casa do tabuleiro. As
             * coordenadas desta casa serão retornadas em um objeto Posicao
             * (coordenadas x e y).
             *
             * A tentativa se seleção é repetida até que o usuário tenha selecionado uma
             * posição válida.
             */

            Posicao posicao = obterPosicaoValidaParaPersonagem(jogo, amigo);

            amigo.setPosicao(posicao);

            // Altera a posição do amigo da vez.
            /*
             * Solicita à interface que o tabuleiro seja atualizado, pois a posição
             * do personagem pode ter sido alterada.
             */
            jogo.atualizaTabuleiro();

            /*
             * Exibe mensagem avisando que o usuário precisa selecionar um oponente
             * a ser atacado pelo amigo da vez.
             */
            jogo.anuncia(String.format("Personagem %s, selecione um inimigo para atacar!", amigo.getNome()));

            /*
             * Solicita um personagem à interface. O usuário deverá selecionar um
             * personagem no tabuleiro (clicando com o mouse sobre o personagem).
             */

            Personagem p = jogo.playerSelecionaPersonagem();
            if (personagemEhInimigo(jogo, p) && !p.equals(amigo)) {
                jogo.executarAtaque(amigo, p);
                if (jogo.acabou()) break;
            } else {
                jogo.anunciaErro("Você não pode atacar você mesmo, nem um amigo. Perdeu a vez!");
            }

            /*
             * Solicita à interface que o tabuleiro seja atualizado, pois os pontos
             * de vida de um personagem podem ter sido alterados.
             */
            jogo.atualizaTabuleiro();
        }
    }

    private Posicao obterPosicaoValidaParaPersonagem(Jogo jogo, PersonagemBase amigo) throws InterruptedException {
        boolean posicaoValidaSelecionada = false;
        Posicao posicao = null;
        while (!posicaoValidaSelecionada) {
            posicao = jogo.selecionaPosicaoDoTabuleiro();
            if (!amigo.alcanca(posicao)) {
                jogo.anunciaErro("Posição Inalcansável");
                continue;
            }
            if (!posicaoEstaLivrePara(jogo, amigo, posicao)) {
                jogo.anunciaErro("Posição Ocupada");
                continue;
            }
            posicaoValidaSelecionada = true;
        }
        return posicao;
    }

    private boolean personagemEhInimigo(Jogo jogo, Personagem personagem) {
        return jogo.personagemEhInimigo(personagem);
    }
}
