package br.ufms.facom.lpoo.rpg.controle.subcontroladores;

import br.ufms.facom.lpoo.rpg.controle.Jogo;

public interface Subcontrolador {
    void executaTurno(Jogo jogo) throws InterruptedException;
}
